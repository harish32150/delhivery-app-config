package com.example.harish.baseproject.ui.login

/**
 * Login States to update UI and logic
 *
 * [Phone_No] - Enter Phone Number State
 * [OTP] - Enter OTP
 * [Authenticated] - User Authenticated, store JWT to user repository
 *
 */
enum class LoginState {
    Phone_No,
    OTP,
    Authenticated
}