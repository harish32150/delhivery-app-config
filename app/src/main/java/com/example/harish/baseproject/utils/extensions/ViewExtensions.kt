package com.example.harish.baseproject.utils.extensions

import android.app.Activity
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable

/**
 * Edit Text Clear text and request focus
 */
fun EditText.clearAndFocus() {
    setText("")
    focus()
}

/**
 * Applied to [EditText] get text and emit if not null and not empty
 * - flat map with chain
 *
 * @return Observable<String>
 */
fun EditText.rxText(): Observable<String> = Observable.just(text.toString().trim())
        .filter { it.isNotNullOrEmpty() }

/**
 * Check for [EditorInfo.IME_ACTION_DONE] editor action and pass text
 *
 * @return Observable<String> with [EditText] text
 */
fun EditText.textImeDone(): Observable<String> =
        RxTextView.editorActions(this)
                .filter { it == EditorInfo.IME_ACTION_DONE }
                .flatMap { rxText() }

/**
 * Focus on edit text
 */
fun EditText.focus() {
    if (requestFocus() && context is Activity) {
        (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    }
}