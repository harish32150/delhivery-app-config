package com.example.harish.baseproject.ui.login

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.example.harish.baseproject.R
import com.example.harish.baseproject.databinding.ActivityLoginBinding
import com.example.harish.baseproject.ui.applist.AppListActivity
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.utils.Logger
import com.example.harish.baseproject.utils.extensions.focus
import com.example.harish.baseproject.utils.extensions.textImeDone

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {
    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun layoutId() = R.layout.activity_login

    override fun requireConnection(): Boolean = true

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* Observer Login state and update same to UI */
        viewModel.loginState.observe(this, Observer {
            when (it) {
                LoginState.Authenticated -> {
                    navigationUtils.navigate(AppListActivity::class.java, finishAfter = true)
                }
                LoginState.Phone_No -> binding.editPhoneNo.focus()
                LoginState.OTP -> binding.editOtp.focus()
            }
            binding.state = it
            binding.notifyChange()
        })

        /* Initialise state */
        viewModel.initState()

        binding.editPhoneNo.textImeDone()
                .mergeWith(binding.editOtp.textImeDone())
                .subscribe({
                    viewModel.onSubmit()
                }, {
                    Logger.loge(this, it)
                })
    }
}