package com.example.harish.baseproject.ui.appconfigdetails

import com.example.harish.baseproject.ui.centerselectiondialog.CenterSelectionDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppConfigDetailsActivityModule {
    @ContributesAndroidInjector
    internal abstract fun provideCenterSelectionDialogFragment(): CenterSelectionDialog
}