package com.example.harish.baseproject.exception

/**
 * Validation Exception
 */
class ValidationException(val errorMessage: String) : Exception(errorMessage)