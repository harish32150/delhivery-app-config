package com.example.harish.baseproject.api.response

import com.google.gson.annotations.SerializedName

/**
 * Login response with [JWTToken]
 */
data class LoginResponse(@SerializedName("jwt") val jwtToken: String)

/**
 * Request OTP Response
 */
data class RequestOTPResponse(val success: String)