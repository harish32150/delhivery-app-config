package com.example.harish.baseproject.injection.module

import android.app.Activity
import android.content.Context
import com.example.harish.baseproject.injection.qualifier.ActivityContext
import com.example.harish.baseproject.injection.scope.ActivityScope
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsActivity
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsActivityModule
import com.example.harish.baseproject.ui.appconfiglist.AppConfigListActivity
import com.example.harish.baseproject.ui.appdetails.AppDetailsActivity
import com.example.harish.baseproject.ui.applist.AppListActivity
import com.example.harish.baseproject.ui.login.LoginActivity
import com.example.harish.baseproject.ui.sample.KotlinActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerAppCompatActivity

/**
 * Activity Binding Modules
 *
 * All Activity specific modules should be declared here along with Abstract Activity Module
 */
@Module
abstract class ActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsKotlinActivityModule::class])
    internal abstract fun bindKotlinActivity(): KotlinActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsLoginActivityModule::class])
    internal abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsAppListActivityModule::class])
    internal abstract fun bindAppListActivity(): AppListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsAppDetailsActivityModule::class])
    internal abstract fun bindAppDetailsActivity(): AppDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsAppConfigListActivityModule::class])
    internal abstract fun bindAppConfigListActivity(): AppConfigListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AbsAppConfigDetailsActivityModule::class, AppConfigDetailsActivityModule::class])
    internal abstract fun bindAppConfigDetailsActivity(): AppConfigDetailsActivity
}

/**
 * Activity common modules,
 * should be created for each activity
 *
 * Activity specific modules should be created separately
 *
 */
@Module
internal abstract class AbsKotlinActivityModule : ActivityModule<KotlinActivity>()

@Module
internal abstract class AbsLoginActivityModule : ActivityModule<LoginActivity>()

@Module
internal abstract class AbsAppListActivityModule : ActivityModule<AppListActivity>()

@Module
internal abstract class AbsAppDetailsActivityModule : ActivityModule<AppDetailsActivity>()

@Module
internal abstract class AbsAppConfigListActivityModule : ActivityModule<AppConfigListActivity>()

@Module
internal abstract class AbsAppConfigDetailsActivityModule : ActivityModule<AppConfigDetailsActivity>()

/**
 * Activity Binds Module
 *
 */
@Module(includes = [BaseActivityModule::class])
internal abstract class ActivityModule<in T : DaggerAppCompatActivity> {
    @Binds
    @ActivityScope
    internal abstract fun bindDaggerAppCompatActivity(activity: T): DaggerAppCompatActivity


    @Binds
    @ActivityScope
    internal abstract fun bindActivity(activity: T): Activity

    @Binds
    @ActivityScope
    @ActivityContext
    internal abstract fun bindContext(activity: T): Context
}

/**
 * Activity Specific common dependencies are provided from here
 */
@Module
class BaseActivityModule {
    //todo - provide Common dependencies like - UIUtils, Navigation Utils etc...
}