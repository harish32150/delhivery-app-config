package com.example.harish.baseproject.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Config data items for global, center, user
 */
@Parcelize
data class ConfigData(
        @SerializedName("logging") val logging: Boolean?,
        @SerializedName("app_name") val appName: String?,
        @SerializedName("location_freq") val locationFreq: Int?,
        @SerializedName("drawer_app") val drawerApp: Boolean?,
        @SerializedName("updated_by") val updatedBy: String?,
        @SerializedName("updated_at") val updatedAt: Long?,
        @SerializedName("version") val version: Int?,
        @SerializedName("install") val install: Boolean?,
        @SerializedName("center") val centerCode: String?,
        @SerializedName("user") val user: String?,    //available with user-config
        var centerName: String?  // added saprately
) : BaseKeyTypeModel<String>(), Parcelable {
    override fun key() = user ?: centerCode ?: "nothing-here"

    fun getDisplayValue() = user ?: centerName ?: "nothing"
}

/**
 * Get dummy add app config data with appname
 *
 * @param appName AppName config has to be added
 *
 * @return [ConfigData]
 */
fun getDummyAddConfigData(appName: String) =
        ConfigData(
                logging = false,
                appName = appName,
                locationFreq = 0,
                drawerApp = false,
                version = 0,
                install = false,
                centerCode = null,
                centerName = null,
                user = null,
                updatedBy = null,
                updatedAt = null
        )