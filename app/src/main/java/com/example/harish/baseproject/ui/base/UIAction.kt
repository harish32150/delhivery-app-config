package com.example.harish.baseproject.ui.base

/**
 * Base UI Action with args
 */
abstract class BaseUIAction<out A : Any>(args: A)


/**
 * Show/Hide soft keyboard action
 *
 * @param show Show/Hide Action
 */
data class SoftKeyboardAction(val show: Boolean = false) : BaseUIAction<Boolean>(show)