package com.example.harish.baseproject.ui.appconfiglist

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.databinding.ViewAppConfigListItemBinding
import com.example.harish.baseproject.ui.base.BaseViewHolder
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter
import com.example.harish.baseproject.utils.DateUtils.formatDate

/**
 * App Config RV Adapter
 */
class AppConfigRVAdapter(clickListener: BaseDataRVAdapter.ItemClickListener<ConfigData>) : BaseDataRVAdapter<ConfigData, ViewAppConfigListItemBinding, AppConfigRVAdapter.AppConfigItemViewHolder>(clickListener) {
    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup) = ViewAppConfigListItemBinding.inflate(inflater, parent, false)

    override fun createVH(binding: ViewAppConfigListItemBinding) = AppConfigItemViewHolder(binding)

    override fun bindVH(holder: AppConfigItemViewHolder, item: ConfigData) {
        holder.binding.config = item

        /* Format date for modified time/date */
        formatDate(item.updatedAt ?: 0, TimeDataFormat).let {
            holder.binding.textModifiedTime.text = it
        }
    }

    /**
     * App Config Item View Holder
     */
    inner class AppConfigItemViewHolder(binding: ViewAppConfigListItemBinding) : BaseViewHolder<ViewAppConfigListItemBinding>(binding)
}

private const val TimeDataFormat = "MMM dd, yyyy hh:mm:ss aaa"