package com.example.harish.baseproject.ui.appconfiglist

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.repository.AppConfigRepository
import com.example.harish.baseproject.ui.base.BaseViewModel
import com.example.harish.baseproject.utils.extensions.not
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.plusAssign
import javax.inject.Inject

class AppConfigListViewModel @Inject constructor(private val appConfigRepository: AppConfigRepository) : BaseViewModel() {

    /* App Config Live Data */
    var configData = MutableLiveData<List<ConfigData>>()

    /**
     * Fetch App Configs
     *
     * @param appName App Name
     * @param type [ConfigType] Config type
     */
    fun fetchConfigs(appName: String, type: ConfigType) {
        compositeDisposable += appConfigRepository.getAppConfig(appName, type)
                .onBackground()
                .progress()
                .subscribe { configRes, error ->
                    if (!error) {
                        configData.postValue(configRes)
                    } else {
                        snackbar(error.message ?: "Error getting $appName's ${type.key} configs")
                    }
                }
    }
}