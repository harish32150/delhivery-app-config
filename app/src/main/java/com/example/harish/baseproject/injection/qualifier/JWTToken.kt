package com.example.harish.baseproject.injection.qualifier

import javax.inject.Qualifier

/**
 * JWT Token Qualifier
 *
 * -- Not using for now as now due to static instance being created.
 */
@Qualifier
@Retention
@Deprecated(message = "Inject AppPrefs and get token", level = DeprecationLevel.ERROR)
annotation class JWTToken