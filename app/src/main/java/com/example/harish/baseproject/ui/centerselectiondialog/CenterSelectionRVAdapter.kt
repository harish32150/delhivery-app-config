package com.example.harish.baseproject.ui.centerselectiondialog

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.harish.baseproject.data.model.CenterData
import com.example.harish.baseproject.databinding.ViewCenterSearchResultBinding
import com.example.harish.baseproject.ui.base.BaseViewHolder
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter

class CenterSelectionRVAdapter(clickListener: ItemClickListener<CenterData>) : BaseDataRVAdapter<CenterData, ViewCenterSearchResultBinding, CenterSelectionRVAdapter.CenterViewHolder>(clickListener) {
    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup) = ViewCenterSearchResultBinding.inflate(inflater, parent, false)

    override fun createVH(binding: ViewCenterSearchResultBinding) = CenterViewHolder(binding)

    override fun bindVH(holder: CenterViewHolder, item: CenterData) {
        holder.binding.centerData = item
    }

    /**
     * Center Selection Item view holder
     */
    class CenterViewHolder(binding: ViewCenterSearchResultBinding) : BaseViewHolder<ViewCenterSearchResultBinding>(binding)
}