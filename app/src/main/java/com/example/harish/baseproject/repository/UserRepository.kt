package com.example.harish.baseproject.repository


import com.example.harish.baseproject.api.UMSService
import com.example.harish.baseproject.api.request.OTPLoginRequest
import com.example.harish.baseproject.api.request.RequestOTPRequest
import com.example.harish.baseproject.exception.ValidationException
import com.example.harish.baseproject.utils.AppPrefs
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * User Repository
 * -- contains most of business logic related to User
 *
 */
@Singleton
class UserRepository @Inject constructor(private val umsService: UMSService, private val appPrefs: AppPrefs) : BaseRepository() {

    fun testRepo() {
        //test function for injection
    }


    /**
     * Request OTP to desired phone no
     *
     * @param phoneNo User Phone number
     */
    fun requestOTP(phoneNo: String?): Single<String> {
        return when {
            phoneNo.isNullOrEmpty() -> Single.error(ValidationException("Phone number is required"))
            phoneNo!!.length != 10 -> Single.error(ValidationException("Phone number should be 10-digits"))
            else -> {
                val request = RequestOTPRequest("+91$phoneNo")
                umsService.requestOTP(request)
                        .map {
                            it.success
                        }
            }
        }
    }

    /**
     * Validate OTP or login by otp, returns a jwt token which will be used for further apis
     *
     * @param phoneNo User Phone number
     * @param otp OTP received
     */
    fun validateOTP(phoneNo: String?, otp: String?): Single<Boolean> {
        return when {
            phoneNo.isNullOrEmpty() -> Single.error(ValidationException("Phone number is required"))
            phoneNo!!.length != 10 -> Single.error(ValidationException("Phone number should be 10-digits"))
            otp.isNullOrEmpty() -> Single.error(ValidationException("OTP is required"))
            otp!!.length != 4 -> Single.error(ValidationException("OTP should be 4-digits"))
            else -> {
                val request = OTPLoginRequest("+91$phoneNo", otp)
                umsService.validateOTP(request)
                        .doOnSuccess {
                            appPrefs.token = it.jwtToken
                        }.map {
                            true
                        }
            }
        }
    }

    /**
     * Check if user is authenticate or token is expired
     *
     * @return [Boolean] true, if authenticated else false. re-authenticate user in case of failed
     *
     * **Note:: [NoSuchElementException] is thrown as return type is void for header call
     */
    fun checkAuthState(): Single<Boolean> =
            if (appPrefs.token.isNullOrEmpty()) {
                Single.just(false)
            } else {
                umsService.checkToken()
                        .map {
                            true
                        }.onErrorReturn {
                            when (it) {
                                is NoSuchElementException -> true
                                else -> {
                                    appPrefs.clearPrefs()
                                    false
                                }
                            }
                        }
            }
}