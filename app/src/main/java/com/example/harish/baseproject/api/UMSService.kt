package com.example.harish.baseproject.api

import com.example.harish.baseproject.api.request.OTPLoginRequest
import com.example.harish.baseproject.api.request.RequestOTPRequest
import com.example.harish.baseproject.api.response.LoginResponse
import com.example.harish.baseproject.api.response.QueryCenterSearchResponse
import com.example.harish.baseproject.api.response.RequestOTPResponse
import com.example.harish.baseproject.api.response.SearchCentreNamesFromCodesResponse
import io.reactivex.Single
import retrofit2.http.*

interface UMSService {
    @POST("request-otp/")
    fun requestOTP(@Body request: RequestOTPRequest): Single<RequestOTPResponse>

    @POST("auth-otp-login/")
    fun validateOTP(@Body request: OTPLoginRequest): Single<LoginResponse>

    @HEAD("v2/applications/6/self/permissions/")
    fun checkToken(): Single<Void>

    @POST("v2/api/centers_fromcodes/")
    fun centerFromCode(@Body codes: MutableList<String>): Single<SearchCentreNamesFromCodesResponse>

    @GET("v2/api/deliverycenter/")
    fun queryCenters(@Query("query_string") query: String): Single<QueryCenterSearchResponse>
}
