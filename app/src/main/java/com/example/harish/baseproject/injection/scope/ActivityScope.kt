package com.example.harish.baseproject.injection.scope

import javax.inject.Scope

@Scope
annotation class ActivityScope