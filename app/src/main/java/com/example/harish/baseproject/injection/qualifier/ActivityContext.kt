package com.example.harish.baseproject.injection.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class ActivityContext