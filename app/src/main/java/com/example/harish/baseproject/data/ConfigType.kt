package com.example.harish.baseproject.data

import android.support.annotation.StringRes
import com.example.harish.baseproject.R
import com.example.harish.baseproject.data.ConfigType.*

/**
 * App - Config types with key and titles
 *
 * [Global] - Global config
 * [Centre] - Centre level configs
 * [User] - User level configs
 */
enum class ConfigType(val key: String, @StringRes val titleRes: Int) {
    Global("global", R.string.title_ctype_global),
    Centre("center", R.string.title_ctype_center),
    User("user", R.string.title_ctype_user)
}