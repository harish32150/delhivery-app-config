package com.example.harish.baseproject.ui.sample

import android.app.DatePickerDialog
import android.os.Bundle
import android.widget.Toast
import com.example.harish.baseproject.R
import com.example.harish.baseproject.databinding.ActivityMainBinding
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.utils.DatePatterns
import com.example.harish.baseproject.utils.DateUtils
import com.example.harish.baseproject.utils.DialogUtils
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class KotlinActivity : BaseActivity<ActivityMainBinding, KotlinViewModel>() {
    override fun requireConnection() = false

    override fun getViewModelClass(): Class<KotlinViewModel> = KotlinViewModel::class.java

    override fun layoutId() = R.layout.activity_main

    @Inject
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* Observe Message live data */
        viewModel.messageLiveData.observe(this, android.arch.lifecycle.Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        binding.btn.setOnClickListener {
            SimpleDateFormat("hh:mm", Locale.getDefault()).format(Date()).let {
                viewModel.emitMessage(it)
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        binding.editDob.setOnClickListener {
            dialogUtils.datePicker(DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                DateUtils.calendarFromDate(year, month, dayOfMonth)
                        .let {
                            return@let DateUtils.formatDate(it.time, DatePatterns.SimpleDateFormat)
                        }.let {
                            binding.editDob.setText(it)
                        }
            })
        }
    }
}
