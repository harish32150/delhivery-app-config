package com.example.harish.baseproject.api.request

import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.google.gson.annotations.SerializedName

// Updated By user
private const val Updated_By = "user"

data class UpdateAppConfigRequest(
        @SerializedName("app_path") val path: String,
        @SerializedName("app_name") val name: String,
        @SerializedName("package_name") val packageName: String,
        @SerializedName("icon_path") val iconPath: String,
        @SerializedName("latest_version") val latestVersion: Int,
        @SerializedName("min_version") val minVersion: Int,
        @SerializedName("icon_hover_path") val iconHoverPath: String,
        @SerializedName("users") val users: String,
        @SerializedName("app_data") val appData: Boolean,
        @SerializedName("data_keys") val dataKeys: String,
        @SerializedName("updated_by") val updatedBy: String
)

/**
 * Get Version update request
 *
 * @param appData [DelhiveryAppData] app data
 * @param latestVersion updated latest version
 * @param minVersion updated min version
 *
 * @return [UpdateAppConfigRequest] request for version updation
 */
fun getVersionRequest(appData: DelhiveryAppData, latestVersion: Int, minVersion: Int) =
        appData.let {
            UpdateAppConfigRequest(
                    path = it.path,
                    name = it.name,
                    packageName = it.packageName,
                    iconPath = it.iconPath,
                    iconHoverPath = it.iconHoverPath,
                    users = it.users,
                    appData = it.appData,
                    dataKeys = it.dataKeys,
                    updatedBy = Updated_By,
                    latestVersion = latestVersion,
                    minVersion = minVersion
            )
        }