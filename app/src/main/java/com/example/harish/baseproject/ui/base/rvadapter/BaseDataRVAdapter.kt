package com.example.harish.baseproject.ui.base.rvadapter

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.harish.baseproject.data.model.BaseKeyTypeModel
import com.example.harish.baseproject.ui.base.BaseViewHolder
import com.example.harish.baseproject.utils.extensions.safeEquals

abstract class BaseDataRVAdapter<D : BaseKeyTypeModel<out Any>, B : ViewDataBinding, VH : BaseViewHolder<B>>(private val clickListener: ItemClickListener<D>)
    : RecyclerView.Adapter<VH>() {

    /* List of items */
    protected val items: MutableList<D> = mutableListOf()

    /**
     * Clear data set and set items and notifyDataSetChanged()
     *
     * @param items [List] of [D] items
     */
    fun setItems(items: List<D>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        items[position].let { item ->
            bindVH(holder, item)
            holder.binding.root.setOnClickListener { clickListener.onItemClicked(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = parent.let {
        getBinding(LayoutInflater.from(it.context), it)
    }.let {
        createVH(it)
    }

    /**
     * Get VH Binding as [B]
     *
     * @param inflater LayoutInflater
     * @param parent View Holder parent
     *
     * @return [ViewDataBinding] of type [B]
     */
    abstract fun getBinding(inflater: LayoutInflater, parent: ViewGroup): B

    /**
     * Create View Holder with binding [B]
     *
     * @param binding of type [B]
     *
     * @return [VH] View Holder
     */
    abstract fun createVH(binding: B): VH

    /**
     * Bind View Holder with [D] data item
     *
     * @param holder View Holder of type [VH]
     * @param item [D] data item
     */
    abstract fun bindVH(holder: VH, item: D)

    /**
     * Update item in list
     *
     * @param updatedItem [D] type item
     *
     * @return [Boolean] to consider if updated item is added to list or not, as matching is taking cared on key
     */
    fun updateItem(updatedItem: D?): Boolean {
        if (updatedItem == null) return false
        items.forEach {
            if (it.key().safeEquals(updatedItem.key())) {
                val index = items.indexOf(it)
                items[index] = updatedItem
                notifyItemChanged(index)
                return true
            }
        }
        return false
    }


    /**
     * RV Adapter item click listener of data item [D]
     */
    interface ItemClickListener<D> {
        fun onItemClicked(item: D)
    }
}