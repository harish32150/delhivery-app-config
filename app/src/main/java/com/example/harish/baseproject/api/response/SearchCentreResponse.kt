package com.example.harish.baseproject.api.response

import com.google.gson.annotations.SerializedName

/**
 * List of results indexed
 */
data class SearchCentreNamesFromCodesResponse(
        @SerializedName("search_results") val results: List<String>
)