package com.example.harish.baseproject.data.model

import com.google.gson.annotations.SerializedName

data class CenterSearchResultItem(
        @SerializedName("_id") val id: String,
        @SerializedName("_index") val index: String,
        @SerializedName("_source") val centerData: CenterSearchResultData
)

data class CenterSearchResultData(
        @SerializedName("name") val centerName: String,
        @SerializedName("pk") val centerCode: String
)