package com.example.harish.baseproject.ui.appconfigdetails

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import com.example.harish.baseproject.R
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.data.model.CenterData
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.data.model.getDummyAddConfigData
import com.example.harish.baseproject.databinding.ActivityAppConfigDetailsBinding
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.ui.centerselectiondialog.CenterSelectionCallback
import com.example.harish.baseproject.ui.centerselectiondialog.CenterSelectionDialog
import com.example.harish.baseproject.utils.DialogUtils
import com.example.harish.baseproject.utils.extensions.focus
import com.example.harish.baseproject.utils.extensions.not
import javax.inject.Inject

class AppConfigDetailsActivity : BaseActivity<ActivityAppConfigDetailsBinding, AppConfigDetailsViewModel>(), CenterSelectionCallback {
    override fun getViewModelClass(): Class<AppConfigDetailsViewModel> = AppConfigDetailsViewModel::class.java

    override fun layoutId() = R.layout.activity_app_config_details

    override fun requireConnection() = true

    @Inject
    lateinit var dialogUtils: DialogUtils

    private var editMenu: MenuItem? = null
    private var doneMenu: MenuItem? = null
    private var deleteMenu: MenuItem? = null
    private var menu: Menu? = null

    /* Store selected center */
    private var selectedCenter = MutableLiveData<CenterData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* Validate intent */
        if (!intent || !intent.hasExtra(IntentAppData) || !intent.hasExtra(IntentConfigType) || !intent.hasExtra(IntentConfigDetailsState)) {
            throw IllegalAccessException("intent should contain $IntentAppData $IntentConfigType and $IntentConfigDetailsState")
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* observer config details state and update ui */
        viewModel.configDetailsState.observe(this, Observer {
            it?.let { state ->
                binding.state = state
                when (state) {
                    AppConfigDetailsState.View -> setEditMode(false)
                    AppConfigDetailsState.Edited -> {
                        binding.configData.let { configData ->
                            setResult(Activity.RESULT_OK)
                        }
                        viewModel.configDetailsState.postValue(AppConfigDetailsState.View)
                    }
                    AppConfigDetailsState.Added, AppConfigDetailsState.Deleted -> {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    AppConfigDetailsState.Edit, AppConfigDetailsState.Add -> setEditMode(true)
                }
            }
        })

        /* observer config type */
        viewModel.configType.observe(this, Observer {
            it?.let { type ->
                binding.configType = type
            }
        })

        /* bind intent vars */
        intent!!.let {
            binding.configData = it.getParcelableExtra(IntentAppData) as ConfigData
            (it.getSerializableExtra(IntentConfigType) as ConfigType).let {
                title = "${binding.configData?.appName}/${it.key}"
                viewModel.configType.postValue(it)
            }
            (it.getSerializableExtra(IntentConfigDetailsState) as AppConfigDetailsState).let {
                viewModel.configDetailsState.postValue(it)
            }

            /* Post default values to UI */
            selectedCenter.postValue(CenterData(binding.configData?.centerCode, binding.configData?.centerName))
        }

        /* Observer center selection and update UI */
        selectedCenter.observe(this, Observer {
            binding.editCenter.setText(it?.name ?: "")
        })

        /* Show Center Selection dialog on center clicked */
        binding.editCenter.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                showCenterSelection()
            }
        }

        /* change menu state based on progress */
        viewModel.progressLiveData.observe(this, Observer {
            it?.let { show ->
                menu?.setGroupEnabled(R.id.menu_grp, !show)
            }
        })
    }

    /**
     * Show Center selection dialog
     */
    private fun showCenterSelection() {
        CenterSelectionDialog().let {
            val transaction = supportFragmentManager.beginTransaction()
            supportFragmentManager.findFragmentByTag(CenterSelectionDialogTag)?.let {
                transaction.remove(it)
            }
            transaction.addToBackStack(null)
            it.show(transaction, CenterSelectionDialogTag)
        }
        binding.editVersion.focus()
    }

    /* set selection callback for CenterSelectionFragment */
    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        if (fragment is CenterSelectionDialog) {
            fragment.setSelectionCallback(this)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_app_config_details, menu)
        editMenu = menu?.findItem(R.id.action_edit)
        doneMenu = menu?.findItem(R.id.action_done)
        deleteMenu = menu?.findItem(R.id.action_delete)

        this.menu = menu

        /* Have to setup explicitly as menu is created after state is posted */
        if (viewModel.configDetailsState.value == AppConfigDetailsState.Add) {
            setEditMode(true)
        }

        if (viewModel.configType.value == ConfigType.Global) {
            deleteMenu?.isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_edit -> {
                viewModel.configDetailsState.postValue(AppConfigDetailsState.Edit)
                true
            }
            R.id.action_done -> {
                return if (selectedCenter.value?.code == null && viewModel.configType != ConfigType.Global) {
                    uiUtils.showSnackbar(R.string.error_select_center)
                    false
                } else {
                    createConfigModel().let {
                        viewModel.changeConfig(it)
                    }
                    true
                }
            }
            R.id.action_delete -> {
                /* Show confirmation dialog to delete config */
                val msg = getString(R.string.msg_delete_app_config, viewModel.configType.value?.key
                        ?: "", binding.configData?.getDisplayValue() ?: "")
                dialogUtils.confirmDialog(R.string.title_delete_app_config, msg) {
                    binding.configData?.let {
                        viewModel.deleteConfig(it.appName!!, it.key())
                    }
                }
                true
            }
            else -> false
        }
    }

    private fun createConfigModel() =
            binding.configData!!.let {
                AppConfigDetailsDataModel(
                        appName = it.appName!!,
                        logging = binding.switchLogging.isChecked,
                        install = binding.switchInstallApp.isChecked,
                        drawerApp = binding.switchDrawerApp.isChecked,
                        locationFreq = Integer.parseInt(binding.editFreq.text.toString().trim()),
                        version = Integer.parseInt(binding.editVersion.text.toString().trim()),
                        centerCode = getSelectedCenterCode(),
                        user = binding.editUserId.text.toString().trim()
                )
            }

    private fun getSelectedCenterCode(): String? =
            when (viewModel.configType.value) {
                ConfigType.Global -> "all"
                else -> selectedCenter.value?.code
            }

    /**
     * Set edit mode for versions
     *
     * hide/show menu items respectively and update state to UI
     *
     * @param isEdit true for edit mode and false for view mode
     */
    private fun setEditMode(isEdit: Boolean) {
        doneMenu?.isVisible = isEdit
        editMenu?.isVisible = !isEdit
        deleteMenu?.isVisible = !isEdit
    }

    override fun onCenterSelected(center: CenterData) {
        selectedCenter.postValue(center)
    }
}

private const val CenterSelectionDialogTag = "center_selection_dialog"

private const val IntentAppData = "i_app_data"
private const val IntentConfigType = "i_config_type"
private const val IntentConfigDetailsState = "i_config_state"

/**
 * App Config details intent for view and edit purpose
 *
 * @param context Activity Context
 * @param configType Config types
 * @param configData Config Data to be shown
 *
 * @return [Intent] to view config details
 */
fun appConfigDetailsIntent(context: Context, configType: ConfigType, configData: ConfigData) =
        Intent(context, AppConfigDetailsActivity::class.java).let {
            it.putExtra(IntentConfigType, configType)
            it.putExtra(IntentAppData, configData)
            it.putExtra(IntentConfigDetailsState, AppConfigDetailsState.View)
        }!!

/**
 * Add App Config Intent
 *
 * @param context Activity Context
 * @param configType Config type
 * @param appName App Name for config is to be added
 *
 * @return [Intent] to add app config
 */
fun addAppConfigIntent(context: Context, configType: ConfigType, appName: String) =
        Intent(context, AppConfigDetailsActivity::class.java).let {
            it.putExtra(IntentConfigType, configType)
            it.putExtra(IntentAppData, getDummyAddConfigData(appName))
            it.putExtra(IntentConfigDetailsState, AppConfigDetailsState.Add)
        }!!