package com.example.harish.baseproject.injection.module

import android.arch.persistence.room.Room
import android.content.Context
import com.example.harish.baseproject.KotlinApp
import com.example.harish.baseproject.database.AppDatabase
import com.example.harish.baseproject.injection.qualifier.ApplicationContext
import com.example.harish.baseproject.utils.Config
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    /**
     * Provide Application Context, when [ApplicationContext] annotation is marked
     */
    @Provides
    @ApplicationContext
    fun provideAppContext(app: KotlinApp): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, Config.AppDatabaseName).build()

    /**
     * Provide JWT token for authentication interceptor
     *
     * @param appPrefs instance to get stored token
     * @return String as JWTToken
     */
//    @Provides
//    @JWTToken
//    fun provideJWTToken(appPrefs: AppPrefs): String? = appPrefs.token
}