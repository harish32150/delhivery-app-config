package com.example.harish.baseproject.ui.centerselectiondialog

import com.example.harish.baseproject.data.model.CenterData

/**
 * Center Selection dialog callback
 */
interface CenterSelectionCallback {

    /**
     * Callback to dismiss dialog and send center back
     */
    fun onCenterSelected(center: CenterData)
}