package com.example.harish.baseproject.api

import com.example.harish.baseproject.api.request.UpdateAppConfigRequest
import com.example.harish.baseproject.api.response.BaseResponse
import com.example.harish.baseproject.api.response.ResponseListWrapper
import com.example.harish.baseproject.api.response.UpdateAppConfigResponse
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.data.model.DelhiveryAppData
import io.reactivex.Single
import retrofit2.http.*

/**
 * AppConfigService
 *
 * All REST Api goes here
 */
interface AppConfigService {
    @GET("apps")
    fun getApps()
            : Single<BaseResponse<ResponseListWrapper<DelhiveryAppData>>>

    @GET("app/{app_name}")
    fun getAppDetails(@Path("app_name") appName: String)
            : Single<BaseResponse<ResponseListWrapper<DelhiveryAppData>>>

    @PUT("app")
    fun updateApp(@Body request: UpdateAppConfigRequest)
            : Single<UpdateAppConfigResponse>

    @GET("configs")
    fun getConfigs(@Query("app_name") appName: String, @Query("config_type") configType: String)
            : Single<BaseResponse<ResponseListWrapper<ConfigData>>>

    @POST("config")
    fun updateConfig(@Body request: Any)
            : Single<UpdateAppConfigResponse>

    @DELETE("config")
    fun deleteConfig(@Query("app_name") appName: String, @Query("config_type") configType: String, @Query("data") deleteKey: String)
            : Single<UpdateAppConfigResponse>
}