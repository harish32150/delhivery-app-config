package com.example.harish.baseproject.ui.applist

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.harish.baseproject.R
import com.example.harish.baseproject.databinding.ActivityAppListBinding
import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.example.harish.baseproject.ui.appdetails.IntentAppData
import com.example.harish.baseproject.ui.appdetails.appDetailsIntent
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter

/* App Details Request code */
private const val ReqCodeAppDetails = 1123

class AppListActivity : BaseActivity<ActivityAppListBinding, AppListViewModel>(), BaseDataRVAdapter.ItemClickListener<DelhiveryAppData> {
    override fun getViewModelClass(): Class<AppListViewModel> = AppListViewModel::class.java

    override fun layoutId() = R.layout.activity_app_list

    override fun requireConnection() = true

    private val adapter = AppListRVAdapter(this)

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* Set-up app list RV */
        binding.rvAppList.let {
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
        }

        /* Observer the app items live data and update adapter */
        viewModel.appItems.observe(this, Observer { items ->
            items?.let {
                adapter.setItems(it)
            }
        })

        /* Finally fetch apps */
        viewModel.fetchApps()
    }

    /**
     * On App Item clicked
     *
     * start App Details Activity
     */
    override fun onItemClicked(item: DelhiveryAppData) {
        startActivityForResult(appDetailsIntent(this, item.name), ReqCodeAppDetails)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode == Activity.RESULT_OK) {
            requestCode == ReqCodeAppDetails -> {
                data?.let {
                    val updatedAppData = it.getParcelableExtra(IntentAppData) as DelhiveryAppData?
                    adapter.updateItem(updatedAppData)
                }
            }
        }
    }
}