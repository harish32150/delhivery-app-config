package com.example.harish.baseproject.injection.component

import com.example.harish.baseproject.KotlinApp
import com.example.harish.baseproject.injection.module.ActivityBindingModule
import com.example.harish.baseproject.injection.module.AppModule
import com.example.harish.baseproject.injection.module.NetworkModule
import com.example.harish.baseproject.injection.module.ViewModelFactoryModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelFactoryModule::class, AndroidSupportInjectionModule::class, ActivityBindingModule::class, NetworkModule::class])
interface AppComponent : AndroidInjector<KotlinApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<KotlinApp>()
}