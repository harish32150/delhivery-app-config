package com.example.harish.baseproject.data.model

/**
 * Base Key-Type model contains a key for unique identification
 *
 * @param key Model key, can be [Any] type defined by [KT]
 */
abstract class BaseKeyTypeModel<KT : Any> {
    abstract fun key(): KT
}

