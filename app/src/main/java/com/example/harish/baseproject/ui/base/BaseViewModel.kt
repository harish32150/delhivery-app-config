package com.example.harish.baseproject.ui.base

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.Observable
import android.databinding.PropertyChangeRegistry
import android.support.annotation.StringRes
import com.example.harish.baseproject.utils.extensions.disposeAndClear
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel(), LifecycleObserver, Observable {

    //Databinding property changes registry
    private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()

    /* Make sure to add all disposables to compositeDisposables to avoid memory leaks and crashes */
    protected val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    /* Base Live data variables to update UI for generic methods */
    var toastLiveData = MutableLiveData<Int>() //Toast live data
    var snackbarLiveData = MutableLiveData<Int>() //Snackbar live data
    var sToastLiveData = MutableLiveData<String>() //Toast live data with String
    var sSnackbarLiveData = MutableLiveData<String>() //Snackbar live data with String
    var progressLiveData = MutableLiveData<Boolean>() //Progress live data

    var uiActionLiveData = MutableLiveData<BaseUIAction<Any>>() //Live data for all other UI Actions

    // Network connection state reference, updated by baseactivity
    var isConnected: Boolean = false

    override fun onCleared() {
        super.onCleared()

        //dispose and clear all running processes
        compositeDisposable.disposeAndClear()
    }

    /**
     * Show Toast message
     *
     * @param resId Toast message resource Id
     */
    protected fun toast(@StringRes resId: Int) = toastLiveData.postValue(resId)

    /**
     * Show Snackbar
     *
     * @param resId Snackbar message resource Id
     */
    protected fun snackbar(@StringRes resId: Int) = snackbarLiveData.postValue(resId)

    /**
     * Show Toast message
     *
     * @param msg Toast message
     */
    protected fun toast(msg: String) = sToastLiveData.postValue(msg)

    /**
     * Show Snackbar
     *
     * @param msg Snackbar message
     */
    protected fun snackbar(msg: String) = sSnackbarLiveData.postValue(msg)


    /**
     * Show/hide progress
     *
     * @param show Show/Hide progress flag, by default show
     */
    protected fun showProgress(show: Boolean = true) = progressLiveData.postValue(show)


    /**
     * Handle progress on any [Single] process chain
     *
     * Show progress onSubscribe and hide finally
     */
    protected fun <T> Single<T>.progress(): Single<T> =
            doOnSubscribe { showProgress() }
                    .doFinally { showProgress(false) }

    /**
     * Show/hide soft keyboard
     *
     * @param show Show/hide
     */
    protected fun softKeyboard(show: Boolean = false) {
        uiActionLiveData.postValue(SoftKeyboardAction(show))
    }


    override fun addOnPropertyChangedCallback(
            callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(
            callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    /**
     * Notifies observers that all properties of this instance have changed.
     */
    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies observers that a specific property has changed. The getter for the
     * property that changes should be marked with the @Bindable annotation to
     * generate a field in the BR class to be used as the fieldId parameter.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }
}