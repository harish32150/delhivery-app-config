package com.example.harish.baseproject.utils.extensions

import com.example.harish.baseproject.api.response.BaseResponse
import com.example.harish.baseproject.api.response.UpdateAppConfigResponse
import com.example.harish.baseproject.exception.APIException
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Single - onBackground()
 * Subscribe on IO thread and observer on Android Main Thread
 */
fun <T> Single<T>.onBackground() = subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

/**
 * Observable - onBackground()
 * Subscribe on IO thread and observer on Android Main Thread
 */
fun <T> Observable<T>.onBackground() = subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())


/**
 * Add Disposables to CompositeDisposable,
 * should be disposed when creater is destroyed
 *
 * @param disposable Disposable to be added to composite disposable
 */
operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}

/**
 * Dispose and clear composite disposable, if not disposed already
 */
fun CompositeDisposable.disposeAndClear() {
    if (!isDisposed) {
        dispose()
        clear()
    }
}

/**
 * Safe dispose, dispose if not already disposed
 */
fun Disposable?.safeDispose() = this?.let {
    if (!it.isDisposed) {
        it.dispose()
    }
}

/**
 * Handle response and based on [BaseResponse.isSuccess] flag,
 * response is passed or exception is thrown
 *
 */
fun <M : Any, T : BaseResponse<M>> Single<T>.convertResponse(): Single<M> =
        map {
            if (it.isSuccess) {
                return@map it.responseData
            } else {
                throw APIException(it.errorCode, it.errorMessage)
            }
        }

/**
 * Handle Update Response
 *
 * @return [Single] of [String] message
 */
fun Single<UpdateAppConfigResponse>.handleUpdateResponse(): Single<String> =
        map {
            if (it.success) {
                return@map it.message
            } else {
                throw APIException(-1, it.message)
            }
        }