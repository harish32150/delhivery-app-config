package com.example.harish.baseproject.ui.base

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.util.Log
import com.example.harish.baseproject.BR
import com.example.harish.baseproject.network.ConnectionLiveData
import com.example.harish.baseproject.utils.NavigationUtils
import com.example.harish.baseproject.utils.UiUtils
import com.example.harish.baseproject.utils.extensions.disposeAndClear
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Base Activity extends [DaggerAppCompatActivity]
 *
 * Binds layout id to binding class
 * Provide ViewModel
 *
 * @property B Generated Binding Class
 * @property VM ViewModelClass, should extend [BaseViewModel]
 *
 */
abstract class BaseActivity<B : ViewDataBinding, VM : BaseViewModel> : DaggerAppCompatActivity(),
        LifecycleObserver {

    protected lateinit var binding: B
    protected lateinit var viewModel: VM

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var uiUtils: UiUtils
    @Inject
    lateinit var connectionLiveData: ConnectionLiveData
    @Inject
    lateinit var navigationUtils: NavigationUtils

    /* Make sure to add all disposables to compositeDisposables to avoid memory leaks and crashes */
    protected val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindContentView(layoutId())
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* Observe on toast live data and show toast */
        viewModel.toastLiveData.observe(this, Observer {
            it?.let { uiUtils.showToast(it) }
        })

        /* Observe on snackbar live data and show snackbar */
        viewModel.snackbarLiveData.observe(this, Observer {
            it?.let { uiUtils.showSnackbar(it) }
        })

        /* Observe on toast live data and show toast */
        viewModel.sToastLiveData.observe(this, Observer {
            it?.let { uiUtils.showToast(it) }
        })

        /* Observe on snackbar live data and show snackbar */
        viewModel.sSnackbarLiveData.observe(this, Observer {
            it?.let { uiUtils.showSnackbar(it) }
        })

        /* Observe on progress live data and show/hide progress */
        viewModel.progressLiveData.observe(this, Observer {
            when (it) {
                true -> uiUtils.showProgress()
                else -> uiUtils.hideProgress()
            }
        })

        /* Observer ui actions */
        viewModel.uiActionLiveData.observe(this, uiActionObserver)

        /* Observer network change state and show No-Internet UI, if internet is required by activity */
        connectionLiveData.observe(this, Observer {
            viewModel.isConnected = it ?: false
            if (requireConnection()) {
                runOnUiThread {
                    if (it == true) {
                        uiUtils.dismissSnackbar()
                    } else {
                        uiUtils.showNoInternetSnackbar()
                    }
                }
            }
        })

        /* ... other Ui observers */
    }

    /** Hide **/
    private fun bindContentView(@LayoutRes layoutId: Int) {
        binding = DataBindingUtil.setContentView(this, layoutId)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClass())
        binding.setVariable(BR.viewModel, viewModel)
    }

    override fun onDestroy() {
        super.onDestroy()

        //dispose and clear all process
        compositeDisposable.disposeAndClear()
    }

    /**
     * View Model Class
     *
     * @return [Class] of [VM]
     */
    abstract fun getViewModelClass(): Class<VM>

    /**
     * Layout Resource Id
     *
     * @return [LayoutRes] Layout Id
     */
    @LayoutRes
    protected abstract fun layoutId(): Int

    /**
     * Define whether internet is required by activity or not
     *
     * @return [Boolean] based on flag, active no internet snackbar will be shown
     */
    protected abstract fun requireConnection(): Boolean

    /**
     * Hide soft keyboard
     */
    protected fun hideKeyboard() {
        uiUtils.hideKeyboard()
    }

    /**
     * Observe UI Actions
     */
    private val uiActionObserver = Observer<BaseUIAction<Any>> {
        when (it) {
            is SoftKeyboardAction -> {
                if (it.show) {
                    uiUtils.showKeyboard()
                } else {
                    uiUtils.hideKeyboard()
                }
            }
            else -> Log.d(BaseActivity::class.java.simpleName, "$it is un-implemented action")
        }
    }
}