package com.example.harish.baseproject.ui.appdetails

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.api.AppConfigService
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.example.harish.baseproject.repository.AppConfigRepository
import com.example.harish.baseproject.ui.base.BaseViewModel
import com.example.harish.baseproject.utils.Logger
import com.example.harish.baseproject.utils.extensions.convertResponse
import com.example.harish.baseproject.utils.extensions.not
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.plusAssign
import javax.inject.Inject

class AppDetailsViewModel @Inject constructor(private val appConfigService: AppConfigService, private val apiConfigRepository: AppConfigRepository) : BaseViewModel() {

    /* AppData with Update State Live data */
    var appData = MutableLiveData<Pair<DelhiveryAppData, Boolean>>()

    /* Global config live data */
    var globalConfigData = MutableLiveData<ConfigData>()

    /**
     * Fetch App Details
     *
     * @param appName AppName from intent
     */
    fun fetchDetails(appName: String) {
        if (!isConnected) return

        compositeDisposable += appConfigService.getAppDetails(appName)
                .convertResponse()
                .onBackground()
                .map {
                    it.first()  //Get only first result, unfortunately its an array :'(
                }.progress()
                .subscribe { appResponse, error ->
                    if (!error) {
                        appData.postValue(Pair(appResponse, false))
                    } else {
                        Logger.loge(this, error)
                        snackbar(error.message ?: "Error getting details")
                    }
                }

    }

    /**
     * Edit Version
     */
    fun editVersion(latestVersion: String, minVersion: String): Boolean {
        softKeyboard()
        if (!isConnected) return false

        val iLatestVersion = latestVersion.toInt()
        val iMinVersion = minVersion.toInt()

        appData.value?.first?.let {
            if (iLatestVersion == it.latestVersion && iMinVersion == it.minVersion) {
                snackbar("No Change in versions")
                return false
            }

            compositeDisposable += apiConfigRepository.editAppVersion(iLatestVersion, iMinVersion, it)
                    .onBackground()
                    .progress()
                    .subscribe { response, error ->
                        if (!error) {
                            snackbar(response.first.message)
                            appData.postValue(Pair(response.second, true))
                        } else {
                            Logger.loge(this, error)
                            snackbar(error.message ?: "Error getting details")
                        }
                    }
        }

        return true
    }

    /**
     * Fetch global config to navigate to global config for app
     */
    fun fetchGlobalConfig(appName: String) {
        if (globalConfigData.value != null) {
            globalConfigData.postValue(globalConfigData.value)
            return
        }
        compositeDisposable += appConfigService.getConfigs(appName, ConfigType.Global.key)
                .convertResponse()
                .onBackground()
                .progress()
                .subscribe { configRes, error ->
                    if (!error) {
                        configRes.first().let {
                            globalConfigData.postValue(it)
                        }
                    } else {
                        snackbar(error.message ?: "Error getting global config")
                    }
                }
    }
}