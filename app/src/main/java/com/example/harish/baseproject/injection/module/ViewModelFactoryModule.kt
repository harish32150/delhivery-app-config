package com.example.harish.baseproject.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.harish.baseproject.injection.scope.ViewModelScope
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsViewModel
import com.example.harish.baseproject.ui.appconfiglist.AppConfigListViewModel
import com.example.harish.baseproject.ui.appdetails.AppDetailsViewModel
import com.example.harish.baseproject.ui.applist.AppListViewModel
import com.example.harish.baseproject.ui.login.LoginViewModel
import com.example.harish.baseproject.ui.sample.KotlinViewModel
import com.example.harish.baseproject.utils.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * View Model Factory Module
 *
 * Each ViewModel should be declared here as bind<#view_model>
 * else [IllegalArgumentException] will be thrown with "Unknown model class $modelClass"
 */
@Module
abstract class ViewModelFactoryModule {
    /**
     * Sample ViewModel, should be removed before moving to production
     */
    @Binds
    @IntoMap
    @ViewModelScope(KotlinViewModel::class)
    abstract fun bindKotlinViewModel(kotlinViewModel: KotlinViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(AppListViewModel::class)
    abstract fun bindAppListViewModel(appListViewModel: AppListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(AppDetailsViewModel::class)
    abstract fun bindAppDetailsViewModel(appDetailsViewModel: AppDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(AppConfigListViewModel::class)
    abstract fun bindAppConfigListViewModel(appConfigListViewModel: AppConfigListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(AppConfigDetailsViewModel::class)
    abstract fun bindAppConfigDetailsViewModel(appConfigDetailsViewModel: AppConfigDetailsViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}