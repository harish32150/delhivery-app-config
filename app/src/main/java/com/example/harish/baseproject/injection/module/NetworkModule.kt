package com.example.harish.baseproject.injection.module

import android.content.Context
import com.example.harish.baseproject.api.AppConfigService
import com.example.harish.baseproject.api.UMSService
import com.example.harish.baseproject.injection.qualifier.ApplicationContext
import com.example.harish.baseproject.network.ConnectionLiveData
import com.example.harish.baseproject.network.JWTTokenInterceptor
import com.example.harish.baseproject.utils.Config
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Network Module contains all dependencies related to Network
 */
@Module
class NetworkModule {

    /**
     * Provide GSON serialization,
     * Inject it where ever required
     * and avoid creating another instances of Gson
     *
     * @return [Gson]
     */
    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().setLenient().create()

    /**
     * Provide Http(OKHttp) client
     *
     * @return [OkHttpClient]
     */
    @Provides
    @Singleton
    fun provideOkHttpClient(jwtTokenInterceptor: JWTTokenInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(jwtTokenInterceptor)
            .build()

    /**
     * Provide Retrofit for specific Base URL
     *
     * @return [Retrofit]
     */
    private fun getRetrofit(gson: Gson, okHttpClient: OkHttpClient, baseUrl: String): Retrofit =
            Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build()

    /**
     * Provide Login Service for Login Apis
     *
     * @return [UMSService]
     */
    @Provides
    @Singleton
    fun provideLoginService(gson: Gson, okHttpClient: OkHttpClient): UMSService = getRetrofit(gson, okHttpClient, Config.UMSUrl).create(UMSService::class.java)

    /**
     * Provide AppConfigService for REST Apis
     *
     * @return [AppConfigService]
     */
    @Provides
    @Singleton
    fun provideApiService(gson: Gson, okHttpClient: OkHttpClient): AppConfigService = getRetrofit(gson, okHttpClient, Config.AppConfigUrl).create(AppConfigService::class.java)


    /**
     * Provides Connection Live Data for listening to internet connection updates
     *
     * @return [ConnectionLiveData]
     */
    @Provides
    @Singleton
    fun provideConnectionLiveData(@ApplicationContext context: Context): ConnectionLiveData = ConnectionLiveData(context)
}