package com.example.harish.baseproject.utils

import android.app.DatePickerDialog
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import com.example.harish.baseproject.injection.scope.ActivityScope
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

@ActivityScope
class DialogUtils @Inject constructor(private val activity: DaggerAppCompatActivity) {

    /**
     * Show Simple date picker and pass callback via [DatePickerDialog.OnDateSetListener]
     *
     * @param listener On Date Set listener [DatePickerDialog.OnDateSetListener]
     * @param calendar Default Selected calendar(date), by default set to today
     */
    fun datePicker(listener: DatePickerDialog.OnDateSetListener, calendar: Calendar = Calendar.getInstance()) {
        DatePickerDialog(activity, -1, listener,
                calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
        ).show()
    }

    /**
     * Show Confirm dialog
     */
    fun confirmDialog(@StringRes titleRes: Int, msgRes: String, positiveAction: () -> Unit) {
        AlertDialog.Builder(activity)
                .setTitle(titleRes)
                .setMessage(msgRes)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    positiveAction()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
    }
}