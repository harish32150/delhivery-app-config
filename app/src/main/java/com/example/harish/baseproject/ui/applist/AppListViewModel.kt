package com.example.harish.baseproject.ui.applist

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.api.AppConfigService
import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.example.harish.baseproject.ui.base.BaseViewModel
import com.example.harish.baseproject.utils.Logger
import com.example.harish.baseproject.utils.extensions.convertResponse
import com.example.harish.baseproject.utils.extensions.not
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.plusAssign
import javax.inject.Inject

class AppListViewModel @Inject constructor(private val appConfigService: AppConfigService) : BaseViewModel() {

    /* Delhivery App Live data list */
    var appItems = MutableLiveData<List<DelhiveryAppData>>()

    /**
     * Fetch All User Apps
     */
    fun fetchApps() {
        if (!isConnected) return

        compositeDisposable += appConfigService.getApps()
                .onBackground()
                .progress()
                .convertResponse()
                .subscribe { appsResponse, error ->
                    if (!error) {
                        appItems.postValue(appsResponse)
                    } else {
                        Logger.loge(this, error)
                        snackbar(error.message ?: "Error getting apps")
                    }
                }
    }
}