package com.example.harish.baseproject.network

import com.example.harish.baseproject.utils.AppPrefs
import com.example.harish.baseproject.utils.Logger
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * OKHttp Interceptor for JWT Authentication,
 * adds JWTToken with each REST Api
 *
 * @param appPrefs Injected for token
 *
 * -- Not a better way to fetch token, as reading app prefs is a costly process.
 */
class JWTTokenInterceptor @Inject constructor(private val appPrefs: AppPrefs) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        chain.request().let {
            val requestBuilder = it.newBuilder()
                    .method(it.method(), it.body())
            appPrefs.token?.let {
                Logger.logd(this, "Adding JWT_Token:: $it")
                requestBuilder.header("Authorization", "Bearer $it")
            }
            return chain.proceed(requestBuilder.build())
        }
    }
}