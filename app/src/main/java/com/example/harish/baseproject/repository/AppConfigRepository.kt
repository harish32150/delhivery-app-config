package com.example.harish.baseproject.repository

import com.example.harish.baseproject.api.AppConfigService
import com.example.harish.baseproject.api.request.getVersionRequest
import com.example.harish.baseproject.api.response.ResponseListWrapper
import com.example.harish.baseproject.api.response.UpdateAppConfigResponse
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.example.harish.baseproject.utils.AppPrefs
import com.example.harish.baseproject.utils.extensions.convertResponse
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * App Config Repository
 * -- contains most of business logic related to app-config
 */
@Singleton
class AppConfigRepository @Inject constructor(private val appConfigService: AppConfigService, private val appDataRepository: AppDataRepository, private val appPrefs: AppPrefs) : BaseRepository() {

    /**
     * Edit App Version and fetch updated info
     *
     * @return [Pair] of [UpdateAppConfigResponse] -> update response and [DelhiveryAppData] -> fetch updated data
     */
    fun editAppVersion(latestVersion: Int, minVersion: Int, appData: DelhiveryAppData): Single<Pair<UpdateAppConfigResponse, DelhiveryAppData>> {
        val request = getVersionRequest(latestVersion = latestVersion, minVersion = minVersion, appData = appData)
        return appConfigService.updateApp(request)
                .flatMap { updateResponse ->
                    appConfigService.getAppDetails(appData.name)
                            .map {
                                Pair(updateResponse, it.responseData.first())
                            }
                }
    }

    /**
     * Get app config based on type global, user or centre
     *
     * @param appName String app name
     * @param type [ConfigType]
     */
    fun getAppConfig(appName: String, type: ConfigType): Single<ResponseListWrapper<ConfigData>> {
        return appConfigService.getConfigs(appName, type.key)
                .convertResponse()
                .flatMap { configData ->
                    val codes = mutableListOf<String>()
                    for (cd in configData) {
                        codes.add(cd.centerCode ?: "")
                    }
                    return@flatMap appDataRepository.codesToCentres(codes)
                            .map { centresMap ->
                                for (cd in configData) {
                                    cd.centerName = centresMap[cd.centerCode] ?: cd.centerCode
                                }
                                return@map configData
                            }
                }
    }
}