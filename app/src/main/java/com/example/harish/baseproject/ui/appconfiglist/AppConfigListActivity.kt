package com.example.harish.baseproject.ui.appconfiglist

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.harish.baseproject.R
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.data.model.ConfigData
import com.example.harish.baseproject.databinding.ActivityAppConfigListBinding
import com.example.harish.baseproject.ui.appconfigdetails.addAppConfigIntent
import com.example.harish.baseproject.ui.appconfigdetails.appConfigDetailsIntent
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter
import com.example.harish.baseproject.utils.extensions.not

class AppConfigListActivity : BaseActivity<ActivityAppConfigListBinding, AppConfigListViewModel>(), BaseDataRVAdapter.ItemClickListener<ConfigData> {
    override fun getViewModelClass(): Class<AppConfigListViewModel> = AppConfigListViewModel::class.java

    override fun layoutId() = R.layout.activity_app_config_list

    override fun requireConnection() = true

    private lateinit var appName: String
    private lateinit var configType: ConfigType

    private val adapter = AppConfigRVAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* Validate intent */
        if (!intent || !intent.hasExtra(IntentAppName) || !intent.hasExtra(IntentConfigType)) {
            throw IllegalAccessException("intent should contain $IntentAppName and $IntentConfigType")
        }
    }


    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* setup recycler view */
        binding.rvAppConfigs.let {
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
        }

        /* Observe live config data and update adapter for same */
        viewModel.configData.observe(this, Observer {
            it?.let { items ->
                adapter.setItems(items)
            }
        })

        /* get fetch params and fetch app configs */
        intent.let {
            appName = it.getStringExtra(IntentAppName)
            configType = it.getSerializableExtra(IntentConfigType) as ConfigType

            setTitle(configType.titleRes)

            //fetch configs
            viewModel.fetchConfigs(appName, configType)
        }
    }

    override fun onItemClicked(item: ConfigData) {
        startActivityForResult(appConfigDetailsIntent(this, configType, item), ReqCodeViewConfig)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_app_config_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_add -> {
                startActivityForResult(addAppConfigIntent(this, configType, appName), ReqCodeAddConfig)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    ReqCodeViewConfig, ReqCodeAddConfig -> viewModel.fetchConfigs(appName, configType)
                    else -> {
                        //nothing
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}

private const val ReqCodeAddConfig = 1223
private const val ReqCodeViewConfig = 1233

private const val IntentAppName = "i_app_name"
private const val IntentConfigType = "i_config_type"

/**
 * Get App Config activity intent
 *
 * @param appName App Name to be shown
 * @param type Config type,
 * ** [ConfigType.Global] not allowed here
 */
fun appConfigIntent(context: Context, appName: String, type: ConfigType) =
        Intent(context, AppConfigListActivity::class.java).let {
            it.putExtra(IntentAppName, appName)
            it.putExtra(IntentConfigType, type)
        }!!