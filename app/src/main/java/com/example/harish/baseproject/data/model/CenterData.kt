package com.example.harish.baseproject.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CenterData(
        val code: String?,
        val name: String?
) : BaseKeyTypeModel<String>(), Parcelable {
    override fun key() = code ?: "nothing"
}