package com.example.harish.baseproject.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.harish.baseproject.database.dao.UserDao
import com.example.harish.baseproject.database.entity.User

/**
 * App Database, place all entities and DAO's here,
 * also update version, incase any updates to entities
 *
 */
@Database(entities = [(User::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}