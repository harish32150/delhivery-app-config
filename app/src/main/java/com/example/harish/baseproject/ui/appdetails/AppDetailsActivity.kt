package com.example.harish.baseproject.ui.appdetails

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.harish.baseproject.R
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.databinding.ActivityAppDetailsBinding
import com.example.harish.baseproject.databinding.ViewAppDetailItemBinding
import com.example.harish.baseproject.ui.appconfigdetails.appConfigDetailsIntent
import com.example.harish.baseproject.ui.appconfiglist.appConfigIntent
import com.example.harish.baseproject.ui.base.BaseActivity
import com.example.harish.baseproject.utils.extensions.not

class AppDetailsActivity : BaseActivity<ActivityAppDetailsBinding, AppDetailsViewModel>() {
    override fun getViewModelClass(): Class<AppDetailsViewModel> = AppDetailsViewModel::class.java

    override fun layoutId() = R.layout.activity_app_details

    override fun requireConnection() = true

    private var editMenu: MenuItem? = null
    private var doneMenu: MenuItem? = null
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* Validate intent */
        if (!intent || !intent.hasExtra(IntentAppName)) {
            throw IllegalAccessException("intent should contain $IntentAppName")
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        /* Init with View State */
        binding.state = AppDetailsState.View

        /* Observer App Data and update UI */
        viewModel.appData.observe(this, Observer { appData ->
            appData?.first?.let { item ->
                binding.appData = item
                fillDetails(arrayOf(
                        Pair("App Path", item.path),
                        Pair("Package Name", item.packageName),
                        Pair("Icon Path", item.iconPath),
                        Pair("Icon Hover Path", item.iconHoverPath),
                        Pair("Users", item.users)
                ))

                /* In-case of update, Send back updated data to list activity */
                if (appData.second) {
                    Intent().let {
                        it.putExtra(IntentAppData, appData.first)
                        setResult(Activity.RESULT_OK, it)
                    }
                }
            }
        })

        /* Observe progress live data and update UI(hide views) */
        viewModel.progressLiveData.observe(this, Observer {
            it?.let { show ->
                if (show) {
                    binding.state = AppDetailsState.Loading
                } else {
                    setEditMode(false)
                }
            }
        })

        /* Observe global config data and navigate to details page */
        viewModel.globalConfigData.observe(this, Observer {
            it?.let { globalConfig ->
                startActivityForResult(appConfigDetailsIntent(this, ConfigType.Global, globalConfig), ReqCodeGlobalConfig)
            }
        })

        /* App Name */
        getAppName().let {
            title = it  //Title as app name
            viewModel.fetchDetails(it)
        }

        /* change menu state based on progress */
        viewModel.progressLiveData.observe(this, Observer {
            it?.let { show ->
                menu?.setGroupEnabled(R.id.menu_grp, !show)
            }
        })
    }

    /**
     * Fill all the details
     *
     * @param
     */
    private fun fillDetails(details: Array<Pair<String, String>>) {
        binding.containerAppDetails.removeAllViews()
        details.forEach { detail ->
            val detailBinding = ViewAppDetailItemBinding.inflate(layoutInflater, binding.containerAppDetails, false)
            detailBinding.label = detail.first
            detailBinding.value = detail.second
            binding.containerAppDetails.addView(detailBinding.root)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_app_details, menu)
        editMenu = menu?.findItem(R.id.action_edit)
        doneMenu = menu?.findItem(R.id.action_done)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            return when (it.itemId) {
                R.id.action_edit -> {
                    setEditMode(true)
                    true
                }
                R.id.action_done -> {
                    viewModel.editVersion(
                            binding.editLatestVer.text.toString(),
                            binding.editMinVer.text.toString()).let { isSuccess ->
                        if (isSuccess) {
                            setEditMode(false)
                        }
                    }
                    true
                }
                R.id.action_config_global -> {
                    viewModel.fetchGlobalConfig(getAppName())
                    true
                }
                R.id.action_config_centre -> {
                    startActivity(appConfigIntent(this, getAppName(), ConfigType.Centre))
                    true
                }
                R.id.action_config_user -> {
                    startActivity(appConfigIntent(this, getAppName(), ConfigType.User))
                    true
                }
                else -> false
            }
        }
        return false
    }

    /**
     * Set edit mode for versions
     *
     * hide/show menu items respectively and update state to UI
     *
     * @param isEdit true for edit mode and false for view mode
     */
    private fun setEditMode(isEdit: Boolean) {
        doneMenu?.isVisible = isEdit
        editMenu?.isVisible = !isEdit
        menu?.setGroupVisible(R.id.menu_grp_configs, !isEdit)
        binding.state = if (isEdit) AppDetailsState.Edit else AppDetailsState.View
    }

    /**
     * Get Current app name
     *
     * @return String as app-name
     */
    private fun getAppName() = intent.getStringExtra(IntentAppName)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                if (requestCode == ReqCodeGlobalConfig) {
                    //todo - reset global config cache
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }
}

/* Global config request code */
private const val ReqCodeGlobalConfig = 1256

/* Intent extra for app data send back */
const val IntentAppData = "i_app_data"

private const val IntentAppName = "i_app_name"

/**
 * Get App Details activity intent
 *
 * @param appName App Name to be shown
 *
 */
fun appDetailsIntent(context: Context, appName: String) =
        Intent(context, AppDetailsActivity::class.java).let {
            it.putExtra(IntentAppName, appName)
        }!!