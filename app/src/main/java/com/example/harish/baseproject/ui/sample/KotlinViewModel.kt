package com.example.harish.baseproject.ui.sample

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.R
import com.example.harish.baseproject.repository.UserRepository
import com.example.harish.baseproject.ui.base.BaseViewModel
import javax.inject.Inject

class KotlinViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    var messageLiveData = MutableLiveData<String>()

    fun emitMessage(msg: String) {
        messageLiveData.postValue(msg)
    }

    var name: String = ""
    var formattedDOB = ""

    /**
     * Add User to database
     */
    fun addUser() {
        if (name.isEmpty()) {
            snackbar(R.string.error_name_required)
            return
        }
        if (formattedDOB.isEmpty()) {
            snackbar(R.string.error_dob_required)
            return
        }

//        compositeDisposable += userRepository
//                .addUser(User(name = name, dob = DateUtils.parseDate(formattedDOB, DatePatterns.SimpleDateFormat).time))
//                .onBackground()
//                .progress()
//                .subscribe { isSuccess, error ->
//                    if (isSuccess && !error) {
//                        snackbar(R.string.msg_user_added)
//                        name = ""
//                        formattedDOB = ""
//                    } else {
//                        error.printStackTrace()
//                    }
//                }
    }

    /**
     * Get all users from Database and print them
     */
    fun getUsers() {
//        compositeDisposable += userRepository.getAllUsers()
//                .onBackground()
//                .progress()
//                .subscribe { users, error ->
//                    if (!error) {
//                        Log.d(this.javaClass.simpleName, "users::count::${users.size}")
//                        users.forEach {
//                            Log.d(this.javaClass.simpleName, "user::id:${it.id}::name:${it.name}::dob:${DateUtils.formatDate(Date(it.dob), DatePatterns.SimpleDateFormat)}")
//                        }
//                    } else {
//                        error.printStackTrace()
//                    }
//                }
    }
}