package com.example.harish.baseproject.utils

import android.util.Log

/**
 * Logger
 */
object Logger {

    /**
     * Log debug
     *
     * @param classX Class reference
     * @param msg debug message
     */
    fun <T> logd(classX: T, msg: String) {
        Log.d(classX.toString(), msg)
    }

    /**
     * Log error(throwable)
     *
     * @param classX Class reference
     * @param throwable Throwable to be logged
     */
    fun <T> loge(classX: T, throwable: Throwable) {
        Log.e(classX.toString(), throwable.message, throwable)
    }

    /**
     * Log error(exception)
     *
     * @param classX Class reference
     * @param exception Exception to be logged
     */
    fun <T> loge(classX: T, exception: Exception) {
        Log.e(classX.toString(), exception.message, exception)
    }
}