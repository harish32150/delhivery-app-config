package com.example.harish.baseproject.ui.appconfigdetails

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.api.AppConfigService
import com.example.harish.baseproject.api.request.getCenterConfigRequestFromModel
import com.example.harish.baseproject.api.request.getUserConfigRequestFromModel
import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.ui.base.BaseViewModel
import com.example.harish.baseproject.utils.extensions.handleUpdateResponse
import com.example.harish.baseproject.utils.extensions.not
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.plusAssign
import javax.inject.Inject

class AppConfigDetailsViewModel @Inject constructor(private val appConfigService: AppConfigService) : BaseViewModel() {
    /* Config type */
    var configType = MutableLiveData<ConfigType>()

    /* app config details state */
    var configDetailsState = MutableLiveData<AppConfigDetailsState>()

    /**
     * Change/add specific config
     *
     * @param configModel [App]
     */
    fun changeConfig(configModel: AppConfigDetailsDataModel) {
        /* Get config change request from config model */
        val request = when (configType.value) {
            ConfigType.Global, ConfigType.Centre -> getCenterConfigRequestFromModel(configModel, ConfigType.Centre)
            ConfigType.User -> getUserConfigRequestFromModel(configModel)
            else -> throw RuntimeException("Invalid config type")
        }

        compositeDisposable += appConfigService.updateConfig(request)
                .onBackground()
                .progress()
                .handleUpdateResponse()
                .subscribe { message, error ->
                    if (!error) {
                        configDetailsState.postValue(
                                when (configDetailsState.value) {
                                    AppConfigDetailsState.Add -> {
                                        toast(message)
                                        AppConfigDetailsState.Added
                                    }
                                    AppConfigDetailsState.Edit -> {
                                        snackbar(message)
                                        AppConfigDetailsState.Edited
                                    }
                                    else -> AppConfigDetailsState.View
                                }
                        )
                    } else {
                        snackbar(error.message ?: "Error updating config")
                    }
                }
    }

    /**
     * Delete config
     *
     * @param appName App Name
     * @param deleteKey Delete Key - centerCode for [ConfigType.Centre] and userId for [ConfigType.User]
     */
    fun deleteConfig(appName: String, deleteKey: String) {
        compositeDisposable += appConfigService.deleteConfig(appName, configType.value?.key!!, deleteKey)
                .onBackground()
                .progress()
                .handleUpdateResponse()
                .subscribe { message, error ->
                    if (!error) {
                        toast(message)
                        configDetailsState.postValue(AppConfigDetailsState.Deleted)
                    } else {
                        snackbar(error?.message ?: "Error deleting config")
                    }
                }
    }
}