package com.example.harish.baseproject.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DelhiveryAppData(
        @SerializedName("app_path") val path: String,
        @SerializedName("app_name") val name: String,
        @SerializedName("package_name") val packageName: String,
        @SerializedName("icon_path") val iconPath: String,
        @SerializedName("latest_version") val latestVersion: Int,
        @SerializedName("min_version") val minVersion: Int,
        @SerializedName("icon_hover_path") val iconHoverPath: String,
        @SerializedName("users") val users: String,
        @SerializedName("app_data") val appData: Boolean,
        @SerializedName("data_keys") val dataKeys: String
) : BaseKeyTypeModel<String>(), Parcelable {
    override fun key() = name
}