package com.example.harish.baseproject.ui.appdetails

/**
 * App Details State
 *
 * [Loading] Loading details or any progress, hide views now
 * [View] In View mode
 * [Edit] Edit min and latest versions mode
 *
 */
enum class AppDetailsState {
    Loading,
    View,
    Edit
}