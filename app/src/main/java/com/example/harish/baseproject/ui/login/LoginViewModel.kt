package com.example.harish.baseproject.ui.login

import android.arch.lifecycle.MutableLiveData
import com.example.harish.baseproject.R
import com.example.harish.baseproject.exception.ValidationException
import com.example.harish.baseproject.repository.UserRepository
import com.example.harish.baseproject.ui.base.BaseViewModel
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.plusAssign
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    /* Login State live data */
    var loginState = MutableLiveData<LoginState>()

    var phoneNumber = ""
    var otp = ""


    /**
     * Reset to init state
     */
    fun initState() {
        compositeDisposable += userRepository.checkAuthState()
                .onBackground()
                .progress()
                .subscribe({
                    val state = if (it == true) LoginState.Authenticated else LoginState.Phone_No
                    loginState.postValue(state)
                }, {
                    loginState.postValue(LoginState.Phone_No)
                })
    }

    /* Action button submit based on current state */
    fun onSubmit() {
        softKeyboard()
        if (!isConnected) return

        when (loginState.value) {
            LoginState.Phone_No -> requestOTP()
            LoginState.OTP -> validateOTP()
            else -> null
        }?.let {
            compositeDisposable += it
        }
    }

    /**
     * Request OTP
     */
    private fun requestOTP() = userRepository.requestOTP(phoneNumber)
            .onBackground()
            .progress()
            .subscribe({
                snackbar(it)
                loginState.postValue(LoginState.OTP)
            }, { handleError(it) })

    /**
     * Validate OTP
     */
    private fun validateOTP() = userRepository.validateOTP(phoneNumber, otp)
            .onBackground()
            .progress()
            .subscribe({
                toast(R.string.msg_login_success)
                loginState.postValue(LoginState.Authenticated)
            }, { handleError(it) })

    /**
     * Handle Error
     */
    private fun handleError(throwable: Throwable) {
        when (throwable) {
            is ValidationException -> toast(throwable.errorMessage)
            else -> snackbar(throwable.message ?: "Unknown error")
        }
    }
}