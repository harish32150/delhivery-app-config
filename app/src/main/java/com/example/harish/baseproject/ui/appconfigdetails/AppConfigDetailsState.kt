package com.example.harish.baseproject.ui.appconfigdetails

import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsState.*

/**
 * App Config Details Screen State
 *
 * [View] -     Simple View State, when user click on any config item
 * [Add] -      Add New Config based on [ConfigType]
 * [Edit] -     Edit Selected config
 * [Added]-     Config Added, navigate back to last screen
 * [Edited]-    Config Modified
 *
 * Flow ->
 *  [View] -> edit menu -> [Edit] -> Submit -> [Edited] -> set result -> [View]
 *  [Add] -> Submit -> [Added] -> Exit activity
 */
enum class AppConfigDetailsState {
    View,
    Add,
    Edit,
    Added,
    Edited,
    Deleted
}