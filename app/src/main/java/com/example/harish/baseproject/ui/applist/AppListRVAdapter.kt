package com.example.harish.baseproject.ui.applist

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.harish.baseproject.databinding.ViewAppListItemBinding
import com.example.harish.baseproject.data.model.DelhiveryAppData
import com.example.harish.baseproject.ui.base.BaseViewHolder
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter

/**
 * App List RV Adapter
 */
class AppListRVAdapter(clicklistener: BaseDataRVAdapter.ItemClickListener<DelhiveryAppData>) : BaseDataRVAdapter<DelhiveryAppData, ViewAppListItemBinding, AppListRVAdapter.AppListItemViewHolder>(clicklistener) {

    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup) = ViewAppListItemBinding.inflate(inflater, parent, false)

    override fun createVH(binding: ViewAppListItemBinding) = AppListItemViewHolder(binding)

    override fun bindVH(holder: AppListItemViewHolder, item: DelhiveryAppData) {
        holder.binding.appData = item
    }

    /**
     * App List Item View Holder
     */
    inner class AppListItemViewHolder(binding: ViewAppListItemBinding) : BaseViewHolder<ViewAppListItemBinding>(binding)
}