package com.example.harish.baseproject.repository

import com.example.harish.baseproject.api.UMSService
import com.example.harish.baseproject.data.model.CenterData
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * App Data repository takes cares of apis and data related to app
 */
@Singleton
class AppDataRepository @Inject constructor(private val umsService: UMSService) : BaseRepository() {

    /**
     * Convert codes to centres
     *
     * @param codes List of Centre codes
     * @return Map<String, String> contains key as Code and value as Centre
     */
    fun codesToCentres(codes: MutableList<String>): Single<Map<String, String>> {
        return umsService.centerFromCode(codes)
                .map { response ->
                    val codeCentresMap = mutableMapOf<String, String>()
                    val centres = response.results
                    for (i in centres.indices) {
                        codeCentresMap[codes[i]] = centres[i]
                    }
                    return@map codeCentresMap
                }
    }

    /**
     * Search Centers by query
     *
     * @param query String query
     * @return List of Pair of code as first and second as center name
     */
    fun searchCenter(query: String): Single<List<CenterData>> {
        return umsService.queryCenters(query)
                .map { rawResponse ->
                    val centersData = mutableListOf<CenterData>()
                    rawResponse.results.forEach { result ->
                        result.centerData.let {
                            centersData.add(CenterData(it.centerCode, it.centerName))
                        }
                    }
                    centersData
                }
    }
}