package com.example.harish.baseproject.api.request

import com.example.harish.baseproject.data.ConfigType
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsDataModel
import com.google.gson.annotations.SerializedName

abstract class BaseConfigRequest

data class CenterConfigRequest(
        @SerializedName("logging") val logging: Boolean,
        @SerializedName("app_name") val appName: String,
        @SerializedName("location_freq") val locationFreq: Int,
        @SerializedName("drawer_app") val drawerApp: Boolean,
        @SerializedName("updated_by") val updatedBy: String = "user",
        @SerializedName("updated_at") val updatedAt: Long,
        @SerializedName("version") val version: Int,
        @SerializedName("install") val install: Boolean,
        @SerializedName("center") val centerCode: String,
        @SerializedName("config_type") val configType: String,
        @SerializedName("custom") val customParams: Map<String, Any>
) : BaseConfigRequest()

/**
 * Get Center Config Request from Config details model
 *
 * @param model [AppConfigDetailsDataModel] for center and global(center = 'all')
 * @return [CenterConfigRequest] request for updating/adding
 */
fun getCenterConfigRequestFromModel(model: AppConfigDetailsDataModel, type: ConfigType) =
        CenterConfigRequest(
                logging = model.logging,
                appName = model.appName,
                locationFreq = model.locationFreq,
                drawerApp = model.drawerApp,
                updatedAt = 0,
                version = model.version,
                install = model.install,
                centerCode = model.centerCode ?: "",
                configType = type.key,
                customParams = mapOf()
        )

data class UserConfigRequest(
        @SerializedName("logging") val logging: Boolean,
        @SerializedName("app_name") val appName: String,
        @SerializedName("location_freq") val locationFreq: Int,
        @SerializedName("drawer_app") val drawerApp: Boolean,
        @SerializedName("updated_by") val updatedBy: String = "user",
        @SerializedName("updated_at") val updatedAt: Long,
        @SerializedName("version") val version: Int,
        @SerializedName("install") val install: Boolean,
        @SerializedName("center") val centerCode: String,
        @SerializedName("user") val user: String,
        @SerializedName("config_type") val configType: String,
        @SerializedName("custom") val customParams: Map<String, Any>
) : BaseConfigRequest()

/**
 * Get User Config Request from Config details model
 *
 * @param model [AppConfigDetailsDataModel] for user
 * @return [UserConfigRequest] request for updating/adding
 */
fun getUserConfigRequestFromModel(model: AppConfigDetailsDataModel) =
        UserConfigRequest(
                logging = model.logging,
                appName = model.appName,
                locationFreq = model.locationFreq,
                drawerApp = model.drawerApp,
                updatedAt = 0,
                version = model.version,
                install = model.install,
                centerCode = model.centerCode ?: "",
                user = model.user!!,
                configType = ConfigType.User.key,
                customParams = mapOf()
        )