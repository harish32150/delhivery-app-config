package com.example.harish.baseproject.ui.base

import android.arch.lifecycle.ViewModelProvider
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject

abstract class BaseDialogFragment<B : ViewDataBinding> : DaggerAppCompatDialogFragment() {
    protected lateinit var binding: B

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        return binding.root
    }

    /**
     * Layout Resource Id
     *
     * @return [LayoutRes] Layout Id
     */
    @LayoutRes
    protected abstract fun layoutId(): Int
}