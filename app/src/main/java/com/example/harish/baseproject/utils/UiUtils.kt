package com.example.harish.baseproject.utils

import android.content.Context
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.harish.baseproject.R
import com.example.harish.baseproject.databinding.LayoutProgressBinding
import com.example.harish.baseproject.injection.scope.ActivityScope
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

@ActivityScope
class UiUtils @Inject constructor(private val activity: DaggerAppCompatActivity) {


    // Activity root view
    private val activityRoot: ViewGroup by lazy {
        activity.findViewById<ViewGroup>(android.R.id.content)
    }

    //Progress View binding
    private val progressBinding: LayoutProgressBinding by lazy {
        val binding = LayoutProgressBinding.inflate(LayoutInflater.from(activity), activityRoot, false)
        binding.root.setOnTouchListener { _, _ -> true }    // <-- Consume touch events to avoid passing touch events through
        binding
    }

    // Snackbar instance
    private var snackbar: Snackbar? = null

    /**
     * Show Simple Toast with [Toast.LENGTH_SHORT] duration
     *
     * @param msg Toast message
     */
    fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    /**
     * Replica of [showToast] with [StringRes]
     *
     * @param resId String resource Id
     */
    fun showToast(@StringRes resId: Int) {
        activity.getString(resId).let {
            showToast(it)
        }
    }

    /**
     * Hide in-line progress
     * Remove [LayoutProgressBinding] Root view from [activityRoot]
     */
    fun hideProgress() {
        activity.runOnUiThread {
            activityRoot.removeView(progressBinding.root)
        }
    }

    /**
     * Show in-line progress
     * Check and remove if already showing and then add and set progress message
     *
     * @param progressMsg Message showing under progress bar
     */
    fun showProgress(progressMsg: String? = null) {
        activity.runOnUiThread {
            //remove progress layout if exist
            progressBinding.root.parent?.let {
                (it as ViewGroup).removeView(progressBinding.root)
            }

            activityRoot.addView(progressBinding.root)
            progressBinding.text = progressMsg
        }
    }

    /**
     * Show Snackbar with string message
     *
     * @param msg String message
     * @param duration Duration of snackbar, by default is [Snackbar.LENGTH_SHORT]
     */
    fun showSnackbar(msg: String, duration: Int = Snackbar.LENGTH_SHORT) {
//        if (snackbar != null) {
//            if (!snackbar!!.isShownOrQueued) {
//                snackbar!!.show()
//            }
//        } else {
        snackbar = Snackbar.make(activityRoot, msg, duration)
        snackbar!!.show()
//        }
    }

    /**
     * Show Snackbar with [StringRes] id
     *
     * @param resId [StringRes] id
     * @param duration Duration of snackbar, by default is [Snackbar.LENGTH_SHORT]
     */
    fun showSnackbar(@StringRes resId: Int, duration: Int = Snackbar.LENGTH_SHORT) {
        showSnackbar(activity.getString(resId), duration)
    }

    /**
     * Show no internet Indefinite snackbar
     */
    fun showNoInternetSnackbar() {
        showSnackbar(R.string.error_no_internet, Snackbar.LENGTH_INDEFINITE)
    }

    /**
     * Dismiss snackbar and assign null
     */
    fun dismissSnackbar() {
        snackbar?.let {
            it.dismiss()
            snackbar = null
        }
    }

    /**
     * Hide Soft keyboard
     */
    fun hideKeyboard() {
        activity.currentFocus?.let {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    /**
     * Show soft keyboard
     */
    fun showKeyboard() {
        TODO("not implemented")
    }
}