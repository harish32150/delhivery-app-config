package com.example.harish.baseproject.injection.component

import com.example.harish.baseproject.injection.module.NetworkModule
import com.example.harish.baseproject.ui.appconfigdetails.AppConfigDetailsViewModel
import com.example.harish.baseproject.ui.appconfiglist.AppConfigListViewModel
import com.example.harish.baseproject.ui.appdetails.AppDetailsViewModel
import com.example.harish.baseproject.ui.applist.AppListViewModel
import com.example.harish.baseproject.ui.login.LoginViewModel
import com.example.harish.baseproject.ui.sample.KotlinViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters
 */
@Singleton
@Component(modules = [NetworkModule::class])
interface ViewModelInjector {

    /**
     * Injects required dependencies into the specified
     */
    fun inject(kotlinViewModel: KotlinViewModel)

    fun inject(loginViewModel: LoginViewModel)

    fun inject(appListViewModel: AppListViewModel)

    fun inject(appDetailsViewModel: AppDetailsViewModel)

    fun inject(appConfigListViewModel: AppConfigListViewModel)

    fun inject(appConfigDetailsViewModel: AppConfigDetailsViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}