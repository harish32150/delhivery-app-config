package com.example.harish.baseproject.api.response

import com.google.gson.annotations.SerializedName

class UpdateAppConfigResponse(
        @SerializedName("message") val message: String,
        @SerializedName("success") val success: Boolean
)