package com.example.harish.baseproject.api.response

import com.example.harish.baseproject.data.model.CenterSearchResultItem
import com.google.gson.annotations.SerializedName

data class QueryCenterSearchResponse(
        @SerializedName("search_results") val results: List<CenterSearchResultItem>
)