package com.example.harish.baseproject.ui.centerselectiondialog

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.harish.baseproject.R
import com.example.harish.baseproject.data.model.CenterData
import com.example.harish.baseproject.databinding.DialogCenterSelectionBinding
import com.example.harish.baseproject.repository.AppDataRepository
import com.example.harish.baseproject.ui.base.BaseDialogFragment
import com.example.harish.baseproject.ui.base.rvadapter.BaseDataRVAdapter
import com.example.harish.baseproject.utils.UiUtils
import com.example.harish.baseproject.utils.extensions.not
import com.example.harish.baseproject.utils.extensions.onBackground
import com.example.harish.baseproject.utils.extensions.safeDispose
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Center Selection Dialog
 */
class CenterSelectionDialog : BaseDialogFragment<DialogCenterSelectionBinding>(), BaseDataRVAdapter.ItemClickListener<CenterData> {

    override fun layoutId() = R.layout.dialog_center_selection

    @Inject
    lateinit var appDataRepository: AppDataRepository
    @Inject
    lateinit var uiUtils: UiUtils

    /* Search Disposable */
    private var searchDisposable: Disposable? = null

    /* Search results live data */
    private val resultsLiveData = MutableLiveData<List<CenterData>>()

    private val adapter = CenterSelectionRVAdapter(this)

    /* Callback interface */
    private var callback: CenterSelectionCallback? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.setTitle("Select Center")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /* Setup RV for search results */
        binding.rvResults.let {
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = adapter
        }

        /* Add text changes watcher to search queries */
        RxTextView.textChanges(binding.editSearchQuery)
                .filter { it.length > 2 }
                .map { it.toString().trim() }
                .distinctUntilChanged()
                .subscribe { query ->
                    searchQuery(query)
                }

        /* Observer on live data */
        resultsLiveData.observe(this, Observer {
            it?.let { items ->
                adapter.setItems(items)
            }
        })

        /* Dispose on cancel search */
        binding.ibtnCancelSearch.setOnClickListener {
            searchDisposable?.dispose()
        }
    }

    /**
     * Search centers based on query
     */
    private fun searchQuery(query: String) {
        /* Dispose current search query api call */
        searchDisposable.safeDispose()

        /* Make new api call */
        searchDisposable = appDataRepository.searchCenter(query)
                .onBackground()
                .doOnSubscribe { binding.setProgress(true) }
                .doFinally { binding.setProgress(false) }
                .subscribe { results, error ->
                    if (!error) {
                        resultsLiveData.postValue(results)
                    } else {
                        uiUtils.showSnackbar(error.message ?: "Error searching centers")
                    }
                }
    }

    /**
     * Set callback for center selection
     *
     * @param listener [CenterSelectionCallback]
     */
    fun setSelectionCallback(listener: CenterSelectionCallback) {
        this.callback = listener
    }

    override fun onItemClicked(item: CenterData) {
        dismiss()
        callback?.onCenterSelected(item)
    }

    override fun onDestroyView() {
        searchDisposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
        super.onDestroyView()
    }
}