package com.example.harish.baseproject.ui.appconfigdetails

class AppConfigDetailsDataModel(
        val logging: Boolean,
        val appName: String,
        val locationFreq: Int,
        val drawerApp: Boolean,
        val updatedBy: String = "user",
        val version: Int,
        val install: Boolean,
        val centerCode: String?,
        val user: String?
)