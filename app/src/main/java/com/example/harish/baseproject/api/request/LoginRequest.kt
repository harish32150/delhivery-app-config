package com.example.harish.baseproject.api.request

import com.google.gson.annotations.SerializedName

/**
 * Request OTP API Request payload
 *
 * @param phoneNumber Mobile number with +91 as country code where OTP is required
 */
data class RequestOTPRequest(@SerializedName("phone_number") val phoneNumber: String)


/**
 * Validate OTP and login by getting JWT token
 *
 * @param phoneNumber Mobile Number
 * @param otp OTP received
 */
data class OTPLoginRequest(@SerializedName("phone_number") val phoneNumber: String, val otp: String)