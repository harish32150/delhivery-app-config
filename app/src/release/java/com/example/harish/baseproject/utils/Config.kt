package com.example.harish.baseproject.utils

/**
 * All Configs stored here,
 * project may contain multiple config files based on flavours
 *
 */
object Config {
    const val AppPrefsFileName = "app_prefs"
    const val AppDatabaseName = "db_name"   //<---todo database name
    const val UMSUrl = "https://api-ums.delhivery.com"
    const val AppConfigUrl = "https://config-api.delhivery.com"
}